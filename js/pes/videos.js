/*
 * Copyright (c) 2020 Rithvik Vibhu
 * 
 * This script can be used with PESU Academy to get all videos of a unit.
 * 
 * To use it:
 * - Go to PESU Academy and log in -> My Courses -> Select Course -> Select Unit (or leave it at Unit 1)
 * - Then in the console, paste this:
 *      $('head').append('<script src="https://h.blek.ga/js/pes/videos.js"></script>')
 *
*/

var videos = []

function collectVideos(courseunitid, subjectid) {
    return new Promise(async function(resolve, reject) {
        var vids = [];
        // https://pesuacademy.com/Academy/a/studentProfilePESUAdmin?url=studentProfilePESUAdmin&controllerMode=6403&actionType=60&selectedData=10348&id=1&unitid=92
        // let res = await fetch(`https://pesuacademy.com/Academy/a/studentProfilePESUAdmin?controllerMode=6403&actionType=44&courseunitid=${courseunitid}&subjectid=${subjectid}`, {credentials: 'same-origin'})
        //let url = `https://pesuacademy.com/Academy/a/studentProfilePESUAdmin?url=studentProfilePESUAdmin&controllerMode=6403&actionType=44&courseunitid=${courseunitid}&subjectid=${subjectid}&coursecontentid=${coursecontentid}&classNo=${classNo}&type=${type}`
	    let url = `/Academy/a/studentProfilePESUAdmin?url=studentProfilePESUAdmin&controllerMode=6403&actionType=60&selectedData=${subjectid}&id=1&unitid=${courseunitid}`
        let res = await fetch(url, {credentials: 'same-origin'})
    let html = await res.text()
    //   console.log(html)
    let dom = new DOMParser().parseFromString(html, 'text/html');
    // 	console.log(dom)
    $(dom).find('.content-type-area').each(function(idx) {
        console.log('Found video', this)
        let title = $(this).find('h4').text().trim()
        let src = $(this).find('iframe').attr('src')
        if (src && src.indexOf('?') != -1) {
            src = src.substring(0, src.indexOf('?'))
        }
        console.log(title, src)
        vids.push({title: title, src: src})
    })
    console.log('vids', vids)
    resolve(vids)
  })
}

var promises = [];
$('#CourseContentId table tbody tr').each(function(idx) {
  var [a, courseunitid, subjectid, coursecontentid, classNo, type] = /\w+\('(\d+)','(\d+)','(\d+)','(\d+)',(\d+)/.exec($(this).attr('onclick'))
//  console.log(courseunitid, subjectid);
//   if (idx > 0) return;
  promises.push(collectVideos(courseunitid, subjectid, coursecontentid, classNo, type))
})

Promise.all(promises).then(values => {
    let merged = [].concat.apply([], values)
    console.table(merged, ['title', 'src'])
    updateTable(merged)
})


//setTimeout(() => {console.table(videos, ['title', 'src'])}, 3000)
// console.table(videos, ['title', 'src'])


function updateTable(data) {
    var $table;
    if ($('#injected-table').length) {
        $table = $('#injected-table').empty()
    } else {
        $table = $('<table id="injected-table" border="1" style="width: 90%;"></table>')
        $table.appendTo('#StudentProfilePESUContent')
    }
    for (let x of data) {
        $table.append(`<tr>
            <td style="padding: 8px 6px;">${x.title}</td>
            <td style="padding: 8px 6px;"><a href="${x.src}">${x.src}</a></td>
        </tr>`)
    }
//     $('body').scrollTo('#injected-table');
    $('html, body').animate({ scrollTop: $('#injected-table').offset().top})
}
